import React from 'react';
import Home, {HomeWithRote} from './pages/Home';
import CreateCertificate, { CreateCertificateWithRote } from './pages/CreateCertificate';
import BoughtCertificates, {BoughtCertificatesWithRote} from './pages/BoughtCertificate';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import LoginPage, { LoginPageWithRoute } from './pages/LoginPage';
import Register from './pages/Register';
import axios, { Axios } from 'axios';
import Code, {CodeWithRoute} from './pages/Code';
import Cookies from 'js-cookie';
import Error, { ErrorWithRoute } from './pages/Error';

const tokenUri = "http://localhost:9090/oauth2/token"
const userInfoUri = "http://localhost:9090/userinfo"
const tokenIntrospectUri = "http://localhost:9090/oauth2/introspect"

// const router = createBrowserRouter([
//     {
        
//         path : "/certificates",
//         element : <HomeWithRote />
//     },
//     {
//         path : "/createCertificate",
//         element : <CreateCertificate/>
//     },
//     {
//         path : "/boughtCertificate",
//         element : <BoughtCertificateWithRote />
//     },
//     {
//         path : "/login",
//         element : <LoginPage/>
//     },
//     {
//         path : "/register",
//         element : <Register/>
//     }

// ])

class App extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            erorrCode : undefined,
            errorMessage : undefined
        }

        this.errorNavigate = this.errorNavigate.bind(this)
    }
    
    render() {
        return (
            <BrowserRouter>
                <Routes>
                    <Route index element={<Navigate to={"/certificates"} />} />
                    <Route path='/certificates' element={<HomeWithRote errorNavigate={this.errorNavigate} refreshToken={this.refreshToken}/>} />
                    <Route path="/createCertificate" element={<CreateCertificateWithRote errorNavigate={this.errorNavigate} refreshToken={this.refreshToken}/>} />
                    <Route path="/boughtCertificate" element={<BoughtCertificatesWithRote errorNavigate={this.errorNavigate} refreshToken={this.refreshToken}/>} />
                    <Route path="/login" element={<LoginPageWithRoute login={this.login} errorNavigate={this.errorNavigate}/>} />
                    <Route path="/register" element={<Register errorNavigate={this.errorNavigate}/>} />
                    <Route path="/code" element={<CodeWithRoute loginCode={this.loginCode} errorNavigate={this.errorNavigate}/>} />
                    <Route path="*" element={<ErrorWithRoute code={this.state.erorrCode} message={this.state.errorMessage}/>} />
                </Routes>
            </BrowserRouter>
            // <RouterProvider  router={router}/>
        )
    }

    refreshToken(navigate){
        if(!Cookies.get("accessToken")){
            return;
        }
        const payload = new FormData()
        payload.append("client_id", "task4_client")
        const errorNavigate = this.errorNavigate

        axios.post(tokenIntrospectUri, payload,{
            headers : {
                'Content-type': 'application/url-form-encoded',
                'Authorization': "Basic dGFzazRfY2xpZW50OnNlY3JldA=="
            },
            params : {
                token : Cookies.get("accessToken")
            }
        }).then((res)=>{
            if(!res.data.active){
                payload.append("client_id", "task4_client")
                payload.append("redirect_uri","http://localhost:3000/code")
                payload.append("grant_type", "refresh_token")
                payload.append("refresh_token", Cookies.get("refreshToken"))

                axios.post(tokenUri,payload,{
                    headers : {
                        'Content-type': 'application/url-form-encoded',
                        'Authorization': "Basic dGFzazRfY2xpZW50OnNlY3JldA=="
                    }
                }).then((res)=>{

                    Cookies.set("accessToken",res.data.access_token)
                    Cookies.set("refreshToken",res.data.refresh_token)

                    axios.get(userInfoUri,{
                        headers : {
                            'Content-type': 'application/url-form-encoded',
                            'Authorization': "Bearer " +  Cookies.get("accessToken")
                        }
                    }).then((res)=>{

                        Cookies.set("userEmail",res.data.email)
                        res.data.authorities.map((i,num)=>{
                            Cookies.set("userAuthority"+(num+1),i.name)
                            Cookies.set("authoritiesCount",num+1)
                        })

                    })
                }).catch(function(error){
                    if(error.response){
                        Cookies.remove("accessToken")
                        Cookies.remove("refreshToken")
                        errorNavigate(error.response.status,"Something went wrong. Try again later, or check whether you in account",navigate)
                    }else if(error.request){
                        errorNavigate(408,"Request timeout",navigate)
                    }
                })
            }
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status,"Something went wrong. Try again later, or check whether you in account",navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })
    }

    errorNavigate(code, message, navigate){
        this.setState({erorrCode : code, errorMessage : message},()=>{
            navigate("/error")
        })
    }
    
}

export default App