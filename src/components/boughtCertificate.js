import React from "react";
import { AiOutlineDelete } from "react-icons/ai";

class BoughtCertificate extends React.Component{
    certificate = this.props.certificate

    render(){
        return(
                    <div className="certificate">

                        <h1>{this.certificate.giftCertificate.name}</h1>
                        
                        <div className="desc">
                            <div>Description: </div>
                            <span>{this.certificate.giftCertificate.description}</span>
                        </div>
                        <div className="info">
                            <div>Duration: </div>
                            <span>{this.certificate.giftCertificate.duration}</span>
                        </div>
                        <div className="desc">
                            <div>Bought date: </div>
                            <span>{this.certificate.buyDate}</span>
                        </div>
                        <div className="info">
                            <div>Create date: </div>
                            <span>{this.certificate.giftCertificate.createDate}</span>
                        </div>
                        <div className="info">
                            <div>Last update date: </div>
                            <span>{this.certificate.giftCertificate.lastUpdateDate}</span>
                        </div>
                        <div className="info">
                            <div>Tags: </div>
                            <span>{this.certificate.giftCertificate.tags.map((j)=>j.name).join(", ")}</span>
                        </div>
                        <div className="info">
                            <div>Current price: </div>
                            <span>{this.certificate.giftCertificate.price}</span>
                        </div>
                        <div className="info">
                            <div>Paid price: </div>
                            <span>{this.certificate.price}</span>
                        </div>
                        <button onClick={()=>this.props.cancel(this.certificate.giftCertificate.id)} className="buy"><AiOutlineDelete color="red"/>Cancel</button>

                    </div>
        )
    }
}

export default BoughtCertificate