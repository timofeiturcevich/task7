import Cookies from "js-cookie";
import React from "react";
import { AiTwotoneEdit, AiOutlineDelete } from "react-icons/ai";
import Tag from "./tag";


class Certificate extends React.Component{
    constructor(props){
        super(props)

        const authorities = []
        for(let i = 1;i<=Cookies.get("authoritiesCount");i++){
            authorities.push(Cookies.get("userAuthority"+i))
        }

        this.state = {
            authorities : authorities,
            edit : false,
            certificate : {
                id : this.props.certificate.id,
                name : undefined,
                description : undefined,
                duration : undefined,
                price : undefined,
                tags : [],
                removeTags : []
            }
        }

        this.changeTag = this.changeTag.bind(this)
    }

    certificate = this.props.certificate

    render(){
        return(
            this.state.edit && this.state.authorities.includes("ADMIN")? 
                <div className="certificate">
                    <div className="input">
                        <input name="name" id="name" type="text" min={1} placeholder={this.certificate.name} onChange={(e)=> this.setState(previousState =>({
                                        certificate : {
                                            ...previousState.certificate,
                                            name : e.target.value
                                        }
                                    }))} required/>
                        <label htmlFor="name">Certificate name: </label>
                    </div>
                    <div className="input">
                        <input name="description" id="description" type="text" min={1} placeholder={this.certificate.description} onChange={(e)=> this.setState(previousState =>({
                                        certificate : {
                                            ...previousState.certificate,
                                            description : e.target.value
                                        }
                                    }))} required/>
                        <label htmlFor="description">Certificate description: </label>
                    </div>
                    <div className="input">
                        <input name="duration" id="duration" type="number" min={1} placeholder={this.certificate.duration} onChange={(e)=> this.setState(previousState =>({
                                        certificate : {
                                            ...previousState.certificate,
                                            duration : e.target.value
                                        }
                                    }))} required/>
                        <label htmlFor="duration">Certificate duration (days): </label>
                    </div>
                    <div className="input">
                        <input name="price" id="price" type="number" min={1} placeholder={this.certificate.price} onChange={(e)=> this.setState(previousState =>({
                                        certificate : {
                                            ...previousState.certificate,
                                            price : e.target.value
                                        }
                                    }))} required/>
                        <label htmlFor="price">Certificate price: </label>
                    </div>
                    <div className="checkboxes">
                        {this.props.tags && this.props.tags.toReversed().map((i)=>(
                            <Tag id={i.id} name={i.name} key={i.name} addTag={this.changeTag} checked={
                                this.certificate.tags.some(el=>el.name===i.name)
                            }/>
                        ))}
                    </div>
                    <button className="buy" onClick={()=>{
                        this.setState(previousState =>({
                            certificate : {
                                id : previousState.certificate.id,
                                name : previousState.certificate.name || this.certificate.name,
                                description : previousState.certificate.description || this.certificate.description,
                                duration : parseInt(previousState.certificate.duration) || this.certificate.duration,
                                price : parseInt(previousState.certificate.price) || this.certificate.price,
                                tags : previousState.certificate.tags || this.certificate.tags,
                                removeTags : previousState.certificate.removeTags || undefined
                            }
                        }),()=>{
                            this.props.update(this.state.certificate)
                            this.setState({edit : false})
                        })
                    }} >Save</button>
                    <button className="buy" onClick={()=>{
                        this.setState(previousState =>({
                            certificate : {
                                id : previousState.certificate.id,
                                name : undefined,
                                description : undefined,
                                duration : undefined,
                                price : undefined,
                                tags : [],
                                removeTags : []
                            }
                        }))
                        this.setState({edit : false})
                    }}>Cancel</button>
                </div> :
            
                <div className="certificate">

                    <h1>{this.certificate.name}</h1>
                    <div className="desc">
                        <div>Description: </div>
                        <span>{this.certificate.description}</span>
                    </div>
                    <div className="info">
                        <div>Duration: </div>
                        <span>{this.certificate.duration}</span>
                    </div>
                    <div className="info">
                        <div>Create date: </div>
                        <span>{this.certificate.createDate}</span>
                    </div>
                    <div className="info">
                        <div>Last update date: </div>
                        <span>{this.certificate.lastUpdateDate}</span>
                    </div>
                    <div className="info">
                        <div>Tags: </div>
                        <span>{this.certificate.tags.map((j)=>j.name).join(", ")}</span>
                    </div>
                    <div className="info">
                        <div>Price: </div>
                        <span>{this.certificate.price}</span>
                    </div>
                    {Cookies.get("accessToken") && <button className="buy" onClick={()=>this.props.buy(this.certificate.id)}>Buy</button>}
                    {(Cookies.get("accessToken") && this.state.authorities.includes("ADMIN")) && <button className="buy" onClick={()=>this.setState({edit : true})}><AiTwotoneEdit/>Edit</button>}
                    {(Cookies.get("accessToken") && this.state.authorities.includes("ADMIN")) && <button className="buy" onClick={()=>this.props.delete(this.certificate.id)} ><AiOutlineDelete color="red"/>Delete</button>}

                </div> 
                
        )
    }

    changeTag(tagId, e){
        if(this.certificate.tags.some(el=>el.id===tagId)){
            this.setState(previousState =>({
                certificate : {
                    ...previousState.certificate,
                    removeTags : !e.target.checked ? 
                            [...previousState.certificate.removeTags, {id : tagId}] :
                            previousState.certificate.removeTags.filter(function(tag){
                                return tag.id !== tagId
                    })
                }
            }))
        }else{
            this.setState(previousState =>({
                certificate : {
                    ...previousState.certificate,
                    tags : e.target.checked ? 
                            [...previousState.certificate.tags, {id : tagId}] :
                            previousState.certificate.tags.filter(function(tag){
                                return tag.id !== tagId
                    })
                }
            }))
        }
    }
    
}

export default Certificate