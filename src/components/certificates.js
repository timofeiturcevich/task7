import React from "react";
import Certificate from "./certificate";

class Certificates extends React.Component{
    render(){
        return(
            <div className="container">
                {this.props.certificates.map((i) => (
                    <Certificate certificate={i} tags={this.props.tags} key={i.id} buy={this.props.buy} update={this.props.update} delete={this.props.delete}/>
                ))}
            </div>
        )
    }
}

export default Certificates