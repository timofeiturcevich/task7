import React from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { useNavigate, useSearchParams } from 'react-router-dom';

const authCodeVerifiersUri = "http://localhost:8081/authCodeVerifiers"

class Login extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            login : undefined,
            password : undefined
        }
    }
    
    render(){
        return (
            <div>
                <div className="login-container">
                    <form className="form-container" onSubmit={e => { e.preventDefault()}}>
                        <div className="form">
                            <div className="inputs">
                                <div className="input">
                                    <input name="login" id="login" type="text" onChange={(e)=> this.setState({ login : e.target.value})} required/>
                                    <label htmlFor="login">Login</label>
                                </div>
                                <div className="input">
                                    <input name="password" id="password" type="password" onChange={(e)=> this.setState({ password : e.target.value})} required/>
                                    <label htmlFor="password">Password</label>
                                </div>
                            </div>

                        </div>
                        <div className="buttons">
                            <button className="button" onClick={()=>this.authCode()}>Log in</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }


    authCode(){
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate
        axios.get(authCodeVerifiersUri)
            .then((res)=>{
                Cookies.set("codeVerifier",res.data.codeVerifier)
                let requestParams = new URLSearchParams({
                    response_type: "code",
                    client_id: "task4_client",
                    redirect_uri: "http://localhost:3000/code",
                    scope: 'openid',
                    code_challenge : res.data.codeChallenge,
                    code_challenge_method : "S256"
                });

                window.location = "http://localhost:9090/oauth2/authorize?" + requestParams;
            }).catch(function(error){
                if(error.response){
                   errorNavigate(error.response.status,error.response.data.errorMessage,navigate)
                }else if(error.request){
                   errorNavigate(408,"Request timeout",navigate)
                }
            })
        
    }
}

export function LoginWithRoute(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<Login navigate={navigate} search={search} errorNavigate={props.errorNavigate}/>)
}

export default Login