import React from 'react';
import { Link, Navigate } from 'react-router-dom';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const logoutUri = "http://localhost:9090/connect/logout"

class Navbar extends React.Component{
    constructor(props){
        super(props)

        const authorities = []
        for(let i = 1;i<=Cookies.get("authoritiesCount");i++){
            authorities.push(Cookies.get("userAuthority"+i))
        }

        this.state = {
            authorities : authorities
        }
    }
    
    render(){
        return (
            <nav className='navbar'>
                <Link to="/certificates?page=1" className='link'>Certificates</Link>
                {this.state.authorities.includes("ADMIN") && <Link to="/createCertificate" className='link'>Create certificate</Link>}
                {!Cookies.get("accessToken") ? 
                    <Link to="/register" className='link'>Register</Link> :
                    <Link to="/boughtCertificate?page=1" className='link'>Bought certificates</Link>
                }
                {!Cookies.get("accessToken") ? 
                    <Link to="/login" className='link'>Login</Link> : 
                    <li className='link' onClick={()=>{
                        Cookies.remove("accessToken")
                        Cookies.remove("refreshToken")
                        Cookies.remove("userEmail")
                        for(let i = 1;i<=Cookies.get("authoritiesCount");i++){
                            Cookies.remove("userAuthority"+i)
                        }
                        Cookies.remove("authoritiesCount")
                        this.setState({authorities : []})
                        this.props.navigate("/certificates")
                    }}>Log out</li>
                }
            </nav>
        )
    }
}

export function NavbarWithRoute(props){
    const navigate = useNavigate()
    return (<Navbar navigate={navigate}/>)
}

export default Navbar