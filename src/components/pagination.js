import React from "react";

class Pagination extends React.Component{
    selected = this.props.pageInfo.number+1
    totalPages = this.props.pageInfo.totalPages

    render(){
        if(this.totalPages<1){
            return (<div></div>)
        }

        return(
            <nav className="pagination">
                {this.selected !== 1 && <li onClick={()=>this.props.changePage(this.selected===1?1:this.selected-1)}>&lt;</li>}

                    <div className="pages">
                        <li key={1} className={1===this.selected?"selected":""} onClick={()=>this.props.changePage(1)}>1</li>

                        {(this.totalPages>9 && this.selected>4) && <li>...</li>}

                        {Array(this.totalPages > 9 ? (this.selected > 4 && this.totalPages-this.selected > 5 ? 5 : 6) : (this.totalPages===1 ? 0 : this.totalPages-2 )).fill(1).map((el,i) => 

                            <li 
                                key={this.selected > 4 ? (this.totalPages-this.selected<6 ? this.totalPages-6+i : this.selected-2+i) : i+2} 
                                className={(this.selected > 4 ? (this.totalPages-this.selected<6 ? this.totalPages-6+i : this.selected-2+i) : i+2)===this.selected?"selected":""} 
                                onClick={()=>this.props.changePage(this.selected > 4 ? (this.totalPages-this.selected<6 ? this.totalPages-6+i : this.selected-2+i) : i+2)}
                            >

                                {this.selected > 4 ? (this.totalPages-this.selected<6 ? this.totalPages-6+i : this.selected-2+i) : i+2}

                            </li>
                        )}













                        {(this.totalPages>9 && this.selected<this.totalPages-5) && <li>...</li>}
                        
                        {this.totalPages>1&&<li key={this.totalPages} className={this.totalPages===this.selected?"selected":""} onClick={()=>this.props.changePage(this.totalPages)}>{this.totalPages}</li>}
                    </div>

                {this.selected !== this.totalPages && <li onClick={()=>this.props.changePage(this.totalPages===this.selected?this.selected:this.selected+1)}>&gt;</li>}
            </nav>
        )
    }
}

export default Pagination