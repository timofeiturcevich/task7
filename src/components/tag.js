import React from "react";

class Tag extends React.Component{

    render(){
        return (
            <div className="option">
                <label htmlFor={this.props.name}>{this.props.name}</label>
                <input type="checkbox" defaultChecked={this.props.checked} id={this.props.name} onChange={(e) => this.props.addTag(this.props.id, e, this.props.name)}/>
            </div>
        )
    }
}

export default Tag