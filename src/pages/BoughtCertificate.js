import React from 'react';
import Pagination from '../components/pagination';
import axios from "axios";
import Navbar, { NavbarWithRoute } from "../components/nvabar";
import { Navigate, useNavigate, useSearchParams } from 'react-router-dom';
import BoughtCertificate from '../components/boughtCertificate';
import Cookies from 'js-cookie';
import { MdDoneOutline, MdOutlineDoNotDisturb } from "react-icons/md";
import { CgSearchLoading } from "react-icons/cg";

const boughtCertificateUrl = "http://localhost:8081/boughtCertificates?size=8&page="
const cancelUrl = "http://localhost:8081/boughtCertificates/cancel"

class BoughtCertificates extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            query : this.props.search.get("page") || "1",
            pageInfo : undefined,
            isLoading : true,
            hidden : true
        }

        this.changePage = this.changePage.bind(this)
        this.cancel = this.cancel.bind(this)
    }

    componentDidMount(){
        if(!Cookies.get("accessToken")){
            return;
        }
        this.props.refreshToken(this.props.navigate)
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.get(boughtCertificateUrl+(this.state.query-1),{
            headers : {
                'Content-type': 'application/json',
                'Authorization': "Bearer " +  Cookies.get("accessToken")
            },
            params : {
                email : Cookies.get("userEmail")
            }
        }).then((res)=>{
            this.setState({pageInfo : res.data})
            
            this.setState({isLoading : false})
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })
    }

    render() {
        if(!Cookies.get("accessToken")){
            return <Navigate to="/certificates" />
        }

        const {isLoading} = this.state

        if(isLoading){
            return ( <div className="container message"><CgSearchLoading />Loading...</div>)
        }

        if(this.state.pageInfo.totalPages===0){
            if(!this.state.hidden){
                return (
                    <div>
                        <NavbarWithRoute />
                        {!this.state.hidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully canceled purchase</div>}
                    </div>
                )
            }

            return (
                <div>
                    <NavbarWithRoute />
                    {!this.state.hidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully canceled purchase</div>}
                    <div className='container message'>
                        <MdOutlineDoNotDisturb color='red'/>No bought certificates found
                    </div>
                </div>
            )
        }

        if(this.state.query > this.state.pageInfo.totalPages){
            return (
                <div>
                    <NavbarWithRoute />
                    <div className='container message'>
                        <MdOutlineDoNotDisturb color='red'/>This page doesnt exists
                    </div>
                    <Pagination pageInfo={this.state.pageInfo} changePage={this.changePage} />
                </div>
            )
        }

        return (
            <div>
                <NavbarWithRoute />
                {!this.state.hidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully canceled purchase</div>}
                <div className='container'>
                    {this.state.pageInfo.content.map((i)=>(
                        <BoughtCertificate certificate={i} key={i.giftCertificate.id} buttonName={"Cancel"} cancel={this.cancel}/>
                    ))}
                </div>
                <Pagination pageInfo={this.state.pageInfo} changePage={this.changePage}/>
            </div>
        )
    }

    cancel(certificateId){
        this.setState({isLoading : true})
        this.props.refreshToken(this.props.navigate)
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate
        const data = {
            certificateId : certificateId,
            email : Cookies.get("userEmail")
        }

        axios.delete(cancelUrl,{
            headers : {
                'Content-type': 'application/json',
                'Authorization': "Bearer " +  Cookies.get("accessToken")
            },
            data : data
        }).then((res)=>{
            this.setState({pageInfo : res.data})
            this.setState({isLoading : false})

            this.setState({hidden : false})
            setTimeout(()=>this.setState({hidden : true}),2000)
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })       
    }


    changePage(page){
        this.setState({isLoading : true})
        this.props.refreshToken(this.props.navigate)
        this.setState({query : page})
        const params = new URLSearchParams()
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate
        
        axios.get(boughtCertificateUrl+(page-1))
            .then((res)=>{
                params.set("page", this.state.query)
                this.setState({pageInfo : res.data})
                this.props.navigate({search : params.toString()})
                this.setState({isLoading : false})
            })
            .catch(function(error){
                if(error.response){
                    errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
                }else if(error.request){
                    errorNavigate(408,"Request timeout",navigate)
                }
            })
    }
}

export function BoughtCertificatesWithRote(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<BoughtCertificates navigate={navigate} search={search} errorNavigate={props.errorNavigate} refreshToken={props.refreshToken}/>)
}

export default BoughtCertificates