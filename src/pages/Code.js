import axios from "axios";
import Cookies from "js-cookie";
import React from "react";
import { Navigate, useNavigate, useSearchParams } from 'react-router-dom';

const tokenUri = "http://localhost:9090/oauth2/token"
const userInfoUri = "http://localhost:9090/userinfo"

class Code extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            code : this.props.search.get("code")
        }
    }

    componentDidMount(){
        this.accessCode(this.state.code, this.props.navigate)
    }

    render(){
        if(Cookies.get("accessToken")){
            return <Navigate to="/certificates" />
        }
        return (
            <div className="container message">
                Loading...
            </div>
        )
    }

    accessCode(code, navigate){
        const errorNavigate = this.props.errorNavigate
        if(code==null){
            errorNavigate("400","No code found. Login again, or try again later", navigate)
            return;
        }

        let payload = new FormData()
        payload.append('client_id', "task4_client")
        payload.append('redirect_uri', "http://localhost:3000/code")
        payload.append('grant_type', 'authorization_code')
        payload.append('code', code)
        payload.append('code_verifier',Cookies.get("codeVerifier"))

        Cookies.remove("codeVerifier")

        axios.post(tokenUri,payload,{
            headers : {
                'Content-type': 'application/url-form-encoded',
                'Authorization': "Basic dGFzazRfY2xpZW50OnNlY3JldA=="
            }
        }).then((res)=>{
            Cookies.remove("codeVerifier")
            Cookies.set("accessToken",res.data.access_token)
            Cookies.set("refreshToken",res.data.refresh_token)
            axios.get(userInfoUri,{
                headers : {
                    'Content-type': 'application/url-form-encoded',
                    'Authorization': "Bearer " +  Cookies.get("accessToken")
                }
            }).then((res)=>{
                Cookies.set("userEmail",res.data.email)

                res.data.authorities.map((i,num)=>{
                    Cookies.set("userAuthority"+(num+1),i.name)
                    Cookies.set("authoritiesCount",num+1)
                })
                navigate("/certificates")
            }).catch(function(error){
                if(error.response){
                   errorNavigate(error.response.status,error.response.data.errorMessage,navigate)
                }else if(error.request){
                   errorNavigate(408,"Request timeout",navigate)
                }
            })
        }).catch(function(error){
            if(error.response){
               errorNavigate(error.response.status,error.response.data.errorMessage,navigate)
            }else if(error.request){
               errorNavigate(408,"Request timeout",navigate)
            }
        })
    }
}

export function CodeWithRoute(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<Code navigate={navigate} search={search} user={props.user} loginCode={props.loginCode} errorNavigate={props.errorNavigate}/>)
}

export default Code