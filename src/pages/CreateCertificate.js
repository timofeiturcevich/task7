import React from "react";
import Tag from "../components/tag";
import Navbar, { NavbarWithRoute } from "../components/nvabar";
import axios from "axios";
import { Navigate } from "react-router-dom";
import Cookies from "js-cookie";
import { MdDoneOutline } from "react-icons/md";
import { IoIosCreate } from "react-icons/io";
import { TbLoader } from "react-icons/tb";
import { useNavigate, useSearchParams } from 'react-router-dom';

const tagUri = "http://localhost:8081/tags/all"
const createCertificateUri = "http://localhost:8081/certificates"

class CreateCertificate extends React.Component{
    constructor(props){
        super(props)

        const authorities = []
        for(let i = 1;i<=Cookies.get("authoritiesCount");i++){
            authorities.push(Cookies.get("userAuthority"+i))
        }

        this.state = {
            authorities : authorities,
            isLoading : true,
            tags : [],
            createCertificate : {
                name : undefined,
                description : undefined,
                duration : undefined,
                price : undefined,
                tags : []
            },
            hidden : true,
            tagName : undefined
        }

        this.addTag = this.addTag.bind(this)
    }

    componentDidMount(){
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.get(tagUri)
            .then((res)=>{
                this.setState({tags : res.data.content})
                
                this.setState({isLoading:false})
            })
            .catch(function(error){
                if(error.response){
                    errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
                }else if(error.request){
                    errorNavigate(408,"Request timeout",navigate)
                }
            })
    }

    render(){
        if(!this.state.authorities.includes("ADMIN")){
            return <Navigate to="/certificates" />
        }

        const {isLoading} = this.state

        if(isLoading){
            return ( <div className="container message"><TbLoader />Loading...</div>)
        }

        return (
            <div>
                <NavbarWithRoute />
                {!this.state.hidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully created certificate</div>}
                <div className="container">
                    <form className="form-container" onSubmit={e => { e.preventDefault()}} ref={(el)=> this.createForm = el}>
                        <div className="form">

                            <div className="inputs">
                                <div className="input">
                                    <input name="name" id="name" type="text" onChange={(e)=> this.setState(previousState =>({
                                        createCertificate : {
                                            ...previousState.createCertificate,
                                            name : e.target.value
                                        }
                                    }))} required/>
                                    <label htmlFor="name">Certificate name: </label>
                                </div>
                                <div className="input">
                                    <input name="description" id="description" type="text" onChange={(e)=> this.setState(previousState =>({
                                        createCertificate : {
                                            ...previousState.createCertificate,
                                            description : e.target.value
                                        }
                                    }))} required/>
                                    <label htmlFor="description">Certificate description: </label>
                                </div>
                                <div className="input">
                                    <input name="duration" id="duration" type="number" min={1} onChange={(e)=> this.setState(previousState =>({
                                        createCertificate : {
                                            ...previousState.createCertificate,
                                            duration : e.target.value
                                        }
                                    }))} required/>
                                    <label htmlFor="duration">Certificate duration (days): </label>
                                </div>
                                <div className="input">
                                    <input name="price" id="price" type="number" min={1} onChange={(e)=> this.setState(previousState =>({
                                        createCertificate : {
                                            ...previousState.createCertificate,
                                            price : e.target.value
                                        }
                                    }))} required/>
                                    <label htmlFor="price">Certificate price: </label>
                                </div>
                            </div>

                            <div className="tags">
                                <input name="tagName" id="tagName" type="text" onChange={(e)=> this.setState({tagName : e.target.value})} ref={(el)=> this.createTag = el} placeholder="New tag name(cannot be same as existing tags)" required/>
                                <button onClick={()=>{
                                    const tagName = this.state.tagName
                                    if(this.createTag.checkValidity() && !(this.state.tags.filter(function(tag){
                                        return tag.name === tagName
                                    }).length>0)){
                                        this.setState(previousState=>({
                                            tags : [...previousState.tags, {id : undefined, name : previousState.tagName}],
                                            tagName : undefined
                                        }))
                                        this.createTag.value = ""
                                    }else{
                                        this.createTag.reportValidity()
                                    }
                                }} type="button"><IoIosCreate/>Create tag</button>
                                <div className="checkboxes">
                                    {this.state.tags && this.state.tags.map((i)=>(
                                            <Tag key={i.name} id={i.id} name={i.name} addTag={this.addTag}/>
                                    ))}
                                </div>
                            </div>
                        </div>
                        
                        <button onClick={()=>{
                            this.createTag.removeAttribute("required")
                            if(this.createForm.checkValidity()){
                                this.createCertificate()
                                this.createForm.reset()
                                this.createTag.setAttribute("required",true)
                            }
                        }}><IoIosCreate/>Create</button>
                    </form>
                </div>
            </div>
        )
    }

    createCertificate(){
        this.setState({isLoading : true})
        this.props.refreshToken(this.props.navigate)
        const body = this.state.createCertificate
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.post(createCertificateUri,body,{
            headers : {
                'Content-type': 'application/json',
                'Authorization': "Bearer " +  Cookies.get("accessToken")
            }
        }).then((res)=>(
            this.setState({isLoading : false})
        )).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })

        this.setState({hidden : false})
        setTimeout(()=>this.setState({hidden : true}),2000)
    }

    addTag(id,e, name){
        this.setState(previousState =>({
            createCertificate : {
                ...previousState.createCertificate,
                tags : e.target.checked ? 
                        [...previousState.createCertificate.tags, {id : id, name : name}] :
                        previousState.createCertificate.tags.filter(function(tag){
                            return tag.name !== name
                })
            }
        }))
    }
}

export function CreateCertificateWithRote(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<CreateCertificate navigate={navigate} search={search} errorNavigate={props.errorNavigate} refreshToken={props.refreshToken}/>)
}

export default CreateCertificate