import React from "react";
import { useNavigate, useSearchParams } from 'react-router-dom';

class Error extends React.Component{
    render(){
        return (
            <div className="error">
                <div className="error-info">
                    <div className="code">
                        {this.props.code || 404}
                    </div>
                    <div className="message">
                        {this.props.message || (this.props.code === 401 && "Unauthorized request. Check if you login in your account") || (this.props.code === 500 && "Internal Server Error") || (this.props.code === 403 && "Forbidden") || "Page not found"}
                    </div>
                </div>
                <button className="home" onClick={()=>this.props.navigate("/certificates")}>Home</button>
            </div>
        )
    }
}

export function ErrorWithRoute(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<Error navigate={navigate} search={search} message={props.message} code={props.code}/>)
}

export default Error