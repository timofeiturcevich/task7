import React from 'react';
import Certificates from '../components/certificates'
import Pagination from '../components/pagination';
import axios from "axios";
import Navbar, { NavbarWithRoute } from "../components/nvabar";
import { useNavigate, useSearchParams } from 'react-router-dom';
import Cookies from 'js-cookie';
import { MdDoneOutline, MdOutlineDoNotDisturb } from "react-icons/md";
import { CgSearchLoading } from "react-icons/cg";

const certificatesUrl = "http://localhost:8081/certificates?size=8&page="
const buyUrl = "http://localhost:8081/user/buyCertificate"
const deleteUrl = "http://localhost:8081/certificates/delete?size=8&page="
const updateUrl = "http://localhost:8081/certificates/update"
const tagUri = "http://localhost:8081/tags/all"

class Home extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            query : this.props.search.get("page") || "1",
            pageInfo : undefined,
            isLoading : true,
            buyHidden : true,
            deleteHidden : true,
            updateHidden : true,
            tags : []
        }

        this.changePage = this.changePage.bind(this)
        this.buy = this.buy.bind(this)
        this.delete = this.delete.bind(this)
        this.update = this.update.bind(this)
    }

    componentDidMount(){
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.get(certificatesUrl+(this.state.query-1),{
            timeout : 10000
        })
            .then((res)=>{
                this.setState({pageInfo : res.data})

                axios.get(tagUri,{
                    timeout : 10000
                })
                    .then((res)=>{
                        this.setState({tags : res.data.content})
                    })
                    .catch(function(error){
                        if(error.response){
                            errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
                        }else if(error.request){
                            errorNavigate(408,"Request timeout",navigate)
                        }
                    })

                this.setState({isLoading:false})
            })
            .catch(function(error){
                if(error.response){
                    errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
                }else if(error.request){
                    errorNavigate(408,"Request timeout",navigate)
                }
            })
    }

    render() {
        const {isLoading} = this.state

        if(isLoading){
            return ( <div className="container message"><CgSearchLoading />Loading...</div>)
        }

        if(this.state.pageInfo.totalPages===0){
            if(!this.state.deleteHidden){
                return(
                    <div>
                        <NavbarWithRoute />
                        {!this.state.deleteHidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully deleted certificate</div>}
                    </div>
                )
            }

            return (
                <div>
                    <NavbarWithRoute />
                    <div className='container message'>
                        <MdOutlineDoNotDisturb color='red'/>No certificates found
                    </div>
                </div>
            )
        }

        if(this.state.query > this.state.pageInfo.totalPages){
            return (
                <div>
                    <NavbarWithRoute />
                    <div className='container message'>
                        <MdOutlineDoNotDisturb color='red'/>This page doesnt exists
                    </div>
                    <Pagination pageInfo={this.state.pageInfo} changePage={this.changePage} />
                </div>
            )
        }

        return (
            <div>
                <NavbarWithRoute />
                {!this.state.updateHidden && <div className='container sucsess'><MdDoneOutline color='lime'/> Saved</div>}
                {!this.state.buyHidden && <div className='container sucsess'><MdDoneOutline color='lime'/> You secsessfully bought certificate</div>}
                {!this.state.deleteHidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully deleted certificate</div>}
                <Certificates certificates={this.state.pageInfo.content} tags={this.state.tags} buy={this.buy} update={this.update} delete={this.delete} />
                <Pagination pageInfo={this.state.pageInfo} changePage={this.changePage} />
            </div>
        )
    }


    changePage(page){
        this.setState({isLoading : true})
        this.setState({query : page})
        const params = new URLSearchParams()
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate
        
        
        axios.get(certificatesUrl+(page-1))
            .then((res)=>{
                params.set("page",this.state.query)
                this.setState({pageInfo:res.data})
                this.props.navigate({search: params.toString()})
                this.setState({isLoading:false})
            })
            .catch(function(error){
                if(error.response){
                    errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
                }else if(error.request){
                    errorNavigate(408,"Request timeout",navigate)
                }
            })
    }


    buy(certificateId){
        this.setState({isLoading : true})
        this.props.refreshToken(this.props.navigate)
        const body = {
            certificateId : certificateId,
            email : Cookies.get("userEmail")
        }
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.post(buyUrl,body,{
            headers : {
                'Content-type': 'application/json',
                'Authorization': "Bearer " +  Cookies.get("accessToken")
            }
        }).then((res)=>{
            this.setState({isLoading : false})
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage || "Something went wrong. Check if you havent already bought this certificate or try again later", navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })

        this.setState({buyHidden : false})
        setTimeout(()=>this.setState({buyHidden : true}),2000)
    }

    update(certificate){
        this.setState({isLoading : true})
        this.props.refreshToken(this.props.navigate)
        const body = certificate
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.patch(updateUrl,body,{
                headers : {
                    'Content-type': 'application/json',
                    'Authorization': "Bearer " +  Cookies.get("accessToken")
                }
        }).then((res)=>{
            this.setState(previousState =>({
                pageInfo : {
                    ...previousState.pageInfo,
                    content : previousState.pageInfo.content.filter(function(certificateTempt){
                                return certificateTempt.id !== res.data.id
                              })
                }
            }),()=>{
                this.setState(previousState => ({
                    pageInfo : {
                        ...previousState.pageInfo,
                        content : [...previousState.pageInfo.content, res.data]
                    },
                    isLoading : false
                }))
                
            })
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })

        this.setState({updateHidden : false})
        setTimeout(()=>this.setState({updateHidden : true}),2000)
    }

    delete(certificateId){
        this.setState({isLoading : true})
        this.props.refreshToken(this.props.navigate)
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        axios.delete(deleteUrl+(this.state.query-1),{
            headers : {
                'Content-type': 'application/json',
                'Authorization': "Bearer " +  Cookies.get("accessToken")
            },
            params : {
                id : certificateId
            }
        }).then((res)=>{
            this.setState({pageInfo : res.data})
            this.setState({isLoading : false})
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })

        this.setState({deleteHidden : false})
        setTimeout(()=>this.setState({deleteHidden : true}),2000)
    }
}

export function HomeWithRote(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<Home navigate={navigate} search={search} errorNavigate={props.errorNavigate} refreshToken={props.refreshToken}/>)
}

export default Home