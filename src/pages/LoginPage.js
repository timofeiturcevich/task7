import React from "react";
import { Navigate } from "react-router-dom";
import axios from "axios";
import Navbar, { NavbarWithRoute } from "../components/nvabar";
import Cookies from "js-cookie";
import { useNavigate, useSearchParams } from 'react-router-dom';

const authCodeVerifiersUri = "http://localhost:8081/authCodeVerifiers"

class LoginPage extends React.Component{
    componentDidMount(){
        this.authCode()
    }

    render(){
        if(Cookies.get("accessToken")){
            return <Navigate to="/certificates" />
        }
        return (
            <div>
                {/* <NavbarWithRoute />
                <LoginWithRoute errorNavigate={this.props.errorNavigate}/> */}
                <div className="container message">
                    Loading...
                </div>
            </div>
        )
    }

    authCode(){
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate
        axios.get(authCodeVerifiersUri)
            .then((res)=>{
                Cookies.set("codeVerifier",res.data.codeVerifier)
                let requestParams = new URLSearchParams({
                    response_type: "code",
                    client_id: "task4_client",
                    redirect_uri: "http://localhost:3000/code",
                    scope: 'openid',
                    code_challenge : res.data.codeChallenge,
                    code_challenge_method : "S256"
                });

                window.location = "http://localhost:9090/oauth2/authorize?" + requestParams;
            }).catch(function(error){
                if(error.response){
                   errorNavigate(error.response.status,error.response.data.errorMessage,navigate)
                }else if(error.request){
                   errorNavigate(408,"Request timeout",navigate)
                }
            })
        
    }
}


export function LoginPageWithRoute(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<LoginPage navigate={navigate} search={search} errorNavigate={props.errorNavigate}/>)
}

export default LoginPage