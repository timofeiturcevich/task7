import React from "react";
import Navbar, { NavbarWithRoute } from "../components/nvabar";
import axios from "axios";
import { Navigate } from "react-router-dom"
import Cookies from "js-cookie";
import { MdDoneOutline } from "react-icons/md";
import { useNavigate, useSearchParams } from 'react-router-dom';

const registerUri = "http://localhost:8081/register"

class Register extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            email : undefined,
            firstName : undefined,
            lastName : undefined,
            password : undefined,
            isLoading : false,
            hidden : true
        }

        this.register = this.register.bind(this)
    }

    render(){
        if(Cookies.get("accessToken")){
            return <Navigate to="/certificates" />
        }
        return (
            <div>
                <NavbarWithRoute />
                {!this.state.hidden && <div className='container sucsess'><MdDoneOutline color='lime'/>You secsessfully registered</div>}
                <div className="login-container">
                    <form className="form-container" onSubmit={e => { e.preventDefault()}} ref={(el)=> this.registerForm = el}>
                        <div className="form">
                            <div className="register-inputs">
                                <div className="input">
                                    <input name="login" id="login" type="text" onChange={(e)=> this.setState({email : e.target.value})} required/>
                                    <label htmlFor="login">Login</label>
                                </div>
                                <div className="input">
                                    <input name="firstName" id="firstName" type="text" onChange={(e) => this.setState({firstName : e.target.value})} required/>
                                    <label htmlFor="firstName">First name</label>
                                </div>
                                <div className="input">
                                    <input name="lastName" id="lastName" type="text" onChange={(e) => this.setState({lastName : e.target.value})} required/>
                                    <label htmlFor="lastName">Last name</label>
                                </div>
                                <div className="input">
                                    <input name="password" id="password" type="password" onChange={(e) => this.setState({password : e.target.value})} required/>
                                    <label htmlFor="password">Password</label>
                                </div>
                            </div>

                        </div>
                        <div className="buttons">
                            <button className="button" onClick={()=>{
                                if(this.registerForm.checkValidity()){
                                    this.register()
                                    this.registerForm.reset()
                                }
                            }}>Register</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
    
    register(){
        this.setState({isLoading : true})
        const navigate = this.props.navigate
        const errorNavigate = this.props.errorNavigate

        const body = {
            email : this.state.email,
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            password : this.state.password
        }
        
        axios.post(registerUri,body,{
            'Content-type': 'application/url-form-encoded'
        }).then((res)=>{
            this.setState({isLoading : false})
        }).catch(function(error){
            if(error.response){
                errorNavigate(error.response.status, error.response.data.errorMessage,navigate)
            }else if(error.request){
                errorNavigate(408,"Request timeout",navigate)
            }
        })
        
        this.setState({hidden : false})
        setTimeout(()=>this.setState({hidden : true}),2000)
    }
}

export function RegisterWithRote(props){
    const navigate = useNavigate()
    const [search, setSearch] = useSearchParams()
    return (<Register navigate={navigate} search={search} errorNavigate={props.errorNavigate}/>)
}

export default Register